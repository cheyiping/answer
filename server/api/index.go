package v1

import (
	"server/model/request"
	"server/model/result"
	"server/service"
	"server/util"
	"strconv"

	"github.com/gin-gonic/gin"
)

// @Tags User
// @Summary 分页获取用户列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param pageSize query int true "每页大小"
// @Param page query int true "页码,"
// @Success 200 {string} string "{"code":200,"data":{},"msg":"获取成功"}"
// @Router /user/list [get]
func GetUserList(c *gin.Context) {
	page, err := strconv.Atoi(c.Query("page"))
	pageSize, err := strconv.Atoi(c.Query("pageSize"))
	err, list, total := service.UserList(page, pageSize)
	if err != nil {
		util.FailWithMessage("获取失败", c)
		return
	}
	util.OkWithDetailed(result.PageResult{
		List:     list,
		Total:    total,
		Page:     page,
		PageSize: pageSize,
	}, "获取成功", c)
}

// @Tags User
// @Summary 获取单个用户
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param name query string true "名字"
// @Success 200 {string} string "{"code":200,"data":{},"msg":"获取成功"}"
// @Router /user/find [get]
func GetFindUser(c *gin.Context) {
	userName := c.Query("name")
	err, data := service.FindUser(userName)
	if err != nil {
		util.FailWithMessage(err.Error(), c)
		return
	}
	util.OkWithDetailed(data, "获取成功", c)
}

// @Tags User
// @Summary 添加用户答题记录
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.RequestUser true "用户名,正确数,错误数"
// @Success 200 {string} string "{"code":200,"data":{},"msg":"添加成功"}"
// @Router /user/recording [post]
func PostRecording(c *gin.Context) {
	data := request.RequestUser{}
	if err := c.ShouldBindJSON(&data); err != nil {
		util.FailWithMessage(err.Error(), c)
		return
	}
	if err := service.AnswerAdd(data); err != nil {
		util.FailWithMessage(err.Error(), c)
		return
	}
	// util.OkWithDetailed(loginData, "登录成功", c)
	util.OkWithMessage("添加成功", c)
}

// @Tags Answer
// @Summary 获取所有题库
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Success 200 {string} string "{"code":200,"data":{},"msg":"获取成功"}"
// @Router /answer/question [get]
func GetQuestion(c *gin.Context) {
	err, data := service.AnswerList()
	if err != nil {
		util.FailWithMessage(err.Error(), c)
		return
	}
	util.OkWithDetailed(data, "获取成功", c)
}
