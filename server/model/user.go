package model

import (
	"server/global"
)

type User struct {
	global.GVA_MODEL
	UserName string `json:"username" gorm:"comment:用户名"`
	Fraction int    `json:"fraction" gorm:"comment:分数"`
}
