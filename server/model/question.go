package model

type Question struct {
	Title   	string `json:"title"`
	Answer  	string `json:"answer"`
	Selected 	string `json:"selected"`
 	Options 	[]List `json:"options"`
}

type List struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}
