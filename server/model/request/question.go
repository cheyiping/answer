package request

type RequestUser struct {
	UserName     string `json:"username" gorm:"comment:用户名"`
	CorrectCount int    `json:"correctCount"`
	ErrorCount   int    `json:"errorCount"`
}
