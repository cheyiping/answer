package result

// Modify GetUserResult structure
type GetUserResult struct {
	ID       uint   `gorm:"primarykey" json:"id"`
	UserName string `json:"username" gorm:"comment:用户名"`
	Fraction int    `json:"fraction" gorm:"comment:分数"`
}

// Modify PageResult structure
type PageResult struct {
	List     []GetUserResult `json:"list"`
	Total    int64           `json:"total"`
	Page     int             `json:"page"`
	PageSize int             `json:"pageSize"`
}
