package core

import (
	"server/initialize"
)

func RunServer() {
	Router := initialize.Routers()
	Router.Run(":9000")
}
