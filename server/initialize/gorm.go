package initialize

import (
	"fmt"
	"os"
	"server/model"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

// SqliteTables 注册数据库表专用
func SqliteTables(db *gorm.DB) {
	err := db.AutoMigrate(
		model.User{},
	)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(0)
	}
}

// GormSqlite 初始化Sqlite数据库
func GormSqlite() *gorm.DB {
	db, err := gorm.Open(sqlite.Open("./answer.db"), gormConfig(true))
	if err != nil {
		panic("failed to connect database")
	} else {
		return db
	}
}

// gormConfig 根据配置决定是否开启日志
func gormConfig(mod bool) *gorm.Config {
	if mod {
		return &gorm.Config{
			Logger:                                   logger.Default.LogMode(logger.Info),
			DisableForeignKeyConstraintWhenMigrating: true,
		}
	} else {
		return &gorm.Config{
			Logger:                                   logger.Default.LogMode(logger.Silent),
			DisableForeignKeyConstraintWhenMigrating: true,
		}
	}
}
