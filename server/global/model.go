package global

import (
	"time"
)

type GVA_MODEL struct {
	ID        uint 			 `gorm:"primarykey" json:"id"`
	CreatedAt time.Time		 `json:"createdAt"`
	UpdatedAt time.Time		 `json:"updatedAt"`
}