package main

import (
	"server/core"
	"server/global"
	"server/initialize"
)

// @title WebSite Example API
// @version 1.0
// @description Server WebSite
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
// @BasePath /api

func main() {
	global.GVA_DB = initialize.GormSqlite()
	initialize.SqliteTables(global.GVA_DB)
	core.RunServer()
}
