package router

import (
	api "server/api"

	"github.com/gin-gonic/gin"
)

func UserRouter(Router *gin.RouterGroup) {
	ApiRouter := Router.Group("user")
	{
		ApiRouter.GET("/list", api.GetUserList)
		ApiRouter.GET("/find", api.GetFindUser)
		ApiRouter.POST("/recording", api.PostRecording)
	}
}
