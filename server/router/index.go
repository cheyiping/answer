package router

import (
	api "server/api"

	"github.com/gin-gonic/gin"
)

func AnswerRouter(Router *gin.RouterGroup) {
	ApiRouter := Router.Group("answer")
	{
		ApiRouter.GET("/question", api.GetQuestion)
	}
}
