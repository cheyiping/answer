package service

import (
	"encoding/json"
	"errors"
	io "io/ioutil"
	"math/rand"
	"server/global"
	"server/model"
	"server/model/request"
	"server/model/result"
	"sync"

	"gorm.io/gorm"
)

/**********************************************************************
*Profiles   查询所有用户
***********************************************************************/
func UserList(page int, pageSize int) (err error, list []result.GetUserResult, total int64) {
	limit := pageSize
	offset := pageSize * (page - 1)
	db := global.GVA_DB.Model(&model.User{})
	var userList []result.GetUserResult
	err = db.Count(&total).Error
	err = db.Limit(limit).Offset(offset).Find(&userList).Error
	return err, userList, total
}

/**********************************************************************
*Profiles   查询单个用户
***********************************************************************/
func FindUser(name string) (err error, user result.GetUserResult) {
	if errors.Is(global.GVA_DB.Table("users").Where("user_name = ?", name).First(&user).Error, gorm.ErrRecordNotFound) {
		return errors.New("未找到用户"), user
	}
	return err, user
}

/**********************************************************************
*Profiles   新增记录
***********************************************************************/
func AnswerAdd(data request.RequestUser) error {
	var user model.User
	if errors.Is(global.GVA_DB.Table("users").Where("user_name = ?", data.UserName).First(&user).Error, gorm.ErrRecordNotFound) {
		user.Fraction = data.CorrectCount * 2
		user.UserName = data.UserName
		err := global.GVA_DB.Create(&user).Error
		if err != nil {
			return errors.New("添加失败")

		}
		return err
	}
	user.Fraction = data.CorrectCount * 2
	user.UserName = data.UserName
	err := global.GVA_DB.Table("users").Where("user_name =  ? ", data.UserName).Updates(&user).Error
	return err
}

/**********************************************************************
*Profiles   查询题库
***********************************************************************/
func AnswerList() (err error, question interface{}) {
	err, data := QueryQuestion("./question.json") //get config struct
	return err, data
}

var file_locker sync.Mutex

func QueryQuestion(filename string) (error, interface{}) {
	var question []model.Question
	file_locker.Lock()
	data, err := io.ReadFile(filename)
	file_locker.Unlock()
	if err != nil {
		return errors.New("read json file error"), question
	}
	datajson := []byte(data)
	err = json.Unmarshal(datajson, &question)
	if err != nil {
		return errors.New("unmarshal json file error"), question
	}
	keyList := make([]*model.Question, len(question))
	for i := 0; i < len(question); i++ {
		keyList[i] = &question[i]
		question[i].Options = RandomString(question[i].Options)
		for item := 0; item < len(question[i].Options); item++ {
			if question[i].Options[item].Value == question[i].Answer {
				question[i].Answer = question[i].Options[item].Name
			}
		}
	}
	return err, question
}

func RandomString(data []model.List) []model.List {
	for i := len(data) - 1; i > 0; i-- {
		num := rand.Intn(i + 1)
		data[i].Value, data[num].Value = data[num].Value, data[i].Value
	}
	return data
}
