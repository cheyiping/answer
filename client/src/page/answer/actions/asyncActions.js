import actions from './actions';
import { request } from '@util/axios-hooks'

export const getAnswer = () => async dispatch => {
    try {
        const article = await request.get(`/answer/question`)
        dispatch(actions.getAnswer.receive(article.data.data))
    } catch (err) {
        return console.error(err)
    }
}

export const updateAnswer = (index, selected) => async dispatch => {
    try {
        dispatch(actions.updateAnswer.request({ index, selected }))
    } catch (err) {
        return console.error(err)
    }
}

export const toggleDrawer = name => async dispatch => {

}

export const closeDrawer = () => async dispatch =>
    dispatch(actions.toggleDrawer(null));