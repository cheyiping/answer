import { createActions } from 'redux-actions';

const prefix = 'ANSWER';

const actionMap = {
    GET_ANSWER: {
        REQUEST: null,
        RECEIVE: null
    },
    UPDATE_ANSWER: {
        REQUEST: null,
        RECEIVE: null
    },
};

export default createActions(actionMap, { prefix });