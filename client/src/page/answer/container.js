import { connect } from 'react-redux';
import './style/answer.css'
import answerActions, { closeDrawer, getAnswer, updateAnswer } from './actions';

import Article from './answer';

const mapStateToProps = ({ answer, unhandledErrors }) => ({
    answer,
    unhandledErrors
});
const { markErrorHandled } = answerActions;
const mapDispatchToProps = { closeDrawer, getAnswer, updateAnswer, markErrorHandled };

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Article);
