import actions from '../actions';
import { handleActions } from 'redux-actions';

export const name = 'ANSWER';

export const initialState = {
    list: [],
};

const reducerMap = {
    [actions.getAnswer.receive]: (state, { payload, error }) => {
        if (error) {
            return initialState;
        }
        return {
            ...state,
            list: payload
        };
    },
    [actions.updateAnswer.request]: (state, { payload, error }) => {
        if (error) {
            return initialState;
        }

        return {
            ...state,
            list: state.list.map((item, key) => key === payload.index ? { ...item, selected: payload.selected } : item)
        };
    },
};

export default handleActions(reducerMap, initialState);
