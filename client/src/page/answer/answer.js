import React, { useRef, useEffect, useState } from 'react';
import { Carousel, Button, Timeline, Radio } from 'antd';

const Article = (props) => {
    const carouselRef = useRef();
    const [currentIndex, setCurrentIndex] = useState(0)
    const { answer, getAnswer, updateAnswer } = props;

    useEffect(() => {
        getAnswer()
    }, [getAnswer])

    const contentStyle = {
        height: '160px',
        color: '#fff',
        lineHeight: '160px',
        textAlign: 'center',
        background: '#364d79',
    };

    const onChange = (index, value) => {
        updateAnswer(index, value)
    }

    useEffect(() => {
        carouselRef && carouselRef.current && carouselRef.current.goTo(currentIndex)
    }, [currentIndex])

    return (
        <div className="answer">
            <div className="answer-left">
                <Carousel ref={carouselRef} dots={false} effect="fade">
                    {
                        Array.isArray(answer.list) && answer.list.length ?
                            answer.list.map((item, index) =>
                                <div key={index} style={contentStyle} className="question">
                                    <div className="title">
                                        {item.title}
                                    </div>
                                    <Radio.Group onChange={(e) => onChange(index, e.target.value)} className="options" >
                                        {Array.isArray(item.options) && item.options.length ?
                                            item.options.map((item, index) =>
                                                <div className="options-item" key={index}>
                                                    <span>{item.name}:</span>
                                                    <Radio key={index} value={item.name}>{item.value}</Radio>
                                                </div>
                                            )
                                            : null
                                        }
                                    </Radio.Group>
                                </div>
                            )
                            : null
                    }
                </Carousel>
                <div className="actions">
                    <Button type="primary" onClick={() => { setCurrentIndex(currentIndex >= 0 ? currentIndex - 1 : answer.list.length - 1) }}>上一题</Button>
                    <Button type="primary" onClick={() => { setCurrentIndex(currentIndex < answer.list.length ? currentIndex + 1 : 0) }}>下一题</Button>
                </div>
            </div>
            <div className="answer-right">
                <Timeline>
                    {
                        Array.isArray(answer.list) && answer.list.length ?
                            answer.list.map((item, index) =>
                                <Timeline.Item color={item.selected ? 'green' : 'gray'} key={index}>题目:{item.title}</Timeline.Item>
                            )
                            : null
                    }
                </Timeline>
                <Button type="primary">提交</Button>
            </div>
        </div>
    );
}

export default Article;
