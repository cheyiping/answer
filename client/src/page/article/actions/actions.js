import { createActions } from 'redux-actions';

const prefix = 'ARTICLE';

const actionMap = {
    ADD_ARTICLE: {
        REQUEST: null,
        RECEIVE: null
    },
    REMOVE_ARTICLE: {
        REQUEST: null,
        RECEIVE: null
    },
    GET_ARTICLE: {
        REQUEST: null,
        RECEIVE: null
    },
    UPDATE_ARTICLE: {
        REQUEST: null,
        RECEIVE: null
    }
};

const actionTypes = ['OPEN_OPTIONS_DRAWER', 'CLOSE_OPTIONS_DRAWER'];

export default createActions(actionMap, ...actionTypes, { prefix });