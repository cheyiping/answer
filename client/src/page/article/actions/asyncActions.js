import actions from './actions';
import { request } from '@util/axios-hooks'

export const getArticle = (page, pageSize) => async dispatch => {
    try {
        const article = await request.get(`/article/list?page=${page}&pageSize=${pageSize}`)
        dispatch(actions.getArticle.receive(article.data.data))
    } catch (err) {
        return console.error(err)
    }
}
export const toggleDrawer = name => async dispatch => {

}

export const closeDrawer = () => async dispatch =>
    dispatch(actions.toggleDrawer(null));