import actions from '../actions';
import { handleActions } from 'redux-actions';

export const name = 'ARTICLE';

export const initialState = {
    dataList: {},
};

const reducerMap = {
    [actions.getArticle.receive]: (state, { payload, error }) => {
        if (error) {
            return initialState;
        }
        return {
            ...state,
            dataList: payload
        };
    },
};

export default handleActions(reducerMap, initialState);
