import { connect } from 'react-redux';

import answerActions, { closeDrawer, getArticle } from './actions';

import Article from './article';

const mapStateToProps = ({ article, unhandledErrors }) => ({
    article,
    unhandledErrors
});
const { markErrorHandled } = answerActions;
const mapDispatchToProps = { closeDrawer, getArticle, markErrorHandled };

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Article);
