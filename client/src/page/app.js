import React from 'react';
import Store from "../store";
import { Provider as ReduxProvider } from 'react-redux';
import Answer from './answer'

const App = (props) => {

    return (
        <ReduxProvider store={Store}>
            <Answer />
        </ReduxProvider>
    );
}

export default App;