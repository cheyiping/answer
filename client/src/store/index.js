import { createStore } from 'redux';

import middleware from '../middleware';
import reducer from '../reducer';
import composeEnhancers from '../util/composeEnhancers';

export default createStore(reducer, composeEnhancers(middleware));
