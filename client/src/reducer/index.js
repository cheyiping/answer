import { combineReducers } from 'redux';

import article from '@article/reducer';
import answer from '@answer/reducer';

export default combineReducers({
    answer,
    article,
});
